package com.demandMatrix.vendorValidation;

import java.util.Map.Entry;
import org.testng.annotations.Test;
import com.demandMatrix.pages.LandingPage;
import com.demandMatrix.utils.BaseWebdriver;
import com.demandMatrix.utils.ExcelTestData;

public class DemandMatrixDemo extends BaseWebdriver {

	@Test(enabled = true,priority=1)
	public void validateVenor() {
		
		/*boolean flag = true;
		int counter =0;*/
		
		LandingPage homePage = new LandingPage();
		
		//outer:
		for(Entry<Object, Object> test:ExcelTestData.getAllDatasFromProperties().entrySet()) {
			//flag =
					homePage.navigateToResultPage(test.getKey().toString()).
			validateResult(test.getKey().toString(),test.getValue().toString());
			//if(!flag) counter++;
		//	if(counter>8)
		//		break outer;
			
		}
			
	}
	
	@Test(alwaysRun=true,priority=2,dependsOnMethods="validateVenor")
	public void genrateExcelFile() {
		ExcelTestData.genrateExcel();
	}
	

}
