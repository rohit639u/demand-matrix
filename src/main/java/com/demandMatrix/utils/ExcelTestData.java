package com.demandMatrix.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.Assert;

public class ExcelTestData {

	protected static final Logger logger = LoggerClass.createLogger();

	static private InputStream excelFileToRead;
	static private Workbook workbook;
	static private Sheet sheet;
	static private int rowStart;
	static private int rowEnd;
	static private Row row;
	static private Cell currentCell;
	static private Cell vendorCell;
	static private String key = new String();
	static private String value = new String();
	static private Properties readDataPropertiesFile = new Properties();
	static private Properties putDataOnProperties = new Properties();

	static {
		executeExcelFile();
	}

	public static void executeExcelFile() {
		Assert.assertTrue(getExcelWorkbook(), "Failrd to create excel file");
		readDataPropertiesFile.clear();
		readDataPropertiesFile = getAllDatasInProerties();
	}

	private static boolean getExcelWorkbook() {

		try {
			excelFileToRead = ExcelTestData.class.getClassLoader().getResourceAsStream(Configuration.getExcelfile());
			logger.info("Creating WorkBook ...");
			workbook = WorkbookFactory.create(excelFileToRead);
			logger.info("Workbook with file name \"" + Configuration.getExcelfile() + "\" Created... ");
			return true;
		} catch (Exception e) {
			System.err.println("Not able to create Workbook due to : " + e);
			System.err.println(e.getMessage());
			Thread.currentThread().stop();
			return false;

		}
	}

	
	private static Properties getAllDatasInProerties() {
		try {
			logger.info("Getting data excel sheet with name \"" + Configuration.getDatasheet() + "\"...");
			sheet = workbook.getSheet(Configuration.getDatasheet());
			logger.info("data with name \"" + Configuration.getDatasheet() + "\"...Created.");

			getFirstAndLastRowsNumber(sheet);

			for (int rowNumber = rowStart; rowNumber <= rowEnd; rowNumber++) {

				row = sheet.getRow(rowNumber);

				if (row == null) {
					// still need to complete this part
					// logger.info("row found row as null");
					continue;
				}

				for (int columnNumber = 0; columnNumber < 6; columnNumber++) {
					currentCell = row.getCell(columnNumber, Row.CREATE_NULL_AS_BLANK);
					if (currentCell.getColumnIndex() == 0) {
						key = currentCell.toString();
					} else if (currentCell.getColumnIndex() == 4) {
						value = currentCell.toString();
					}

				}
				if (key != null && value != null) {
					putDataOnProperties.put(key, value);
				}
			}

			if (putDataOnProperties.isEmpty()) {
				logger.info("Excel data in Properties file is empty...");
				throw new NullPointerException();
			}
			return putDataOnProperties;
		} catch (NullPointerException e) {

			logger.info(e.getMessage());
			Thread.currentThread().stop();
			return null;
		}
	}

	private static void getFirstAndLastRowsNumber(Sheet currentSheet) {
		logger.info("Getting rows number.");
		if (currentSheet == null) {
			logger.info("not able to get Rows as sheet is null");
		}
		rowStart = currentSheet.getFirstRowNum() + 1;

		rowEnd = currentSheet.getLastRowNum();
		int endRowTemp = rowEnd + 1;
		logger.info("rows created starting row is at : " + rowStart + ", End row is :" + endRowTemp);
	}

	public static Properties getAllDatasFromProperties() {
		return putDataOnProperties;
	}
	
	public static void writeInExcel(String toCheck, String toWrite) {
		sheet = workbook.getSheet(Configuration.getDatasheet());
		// getFirstAndLastRowsNumber(sheet);
		outer: for (int rowNumber = rowStart; rowNumber <= rowEnd; rowNumber++) {
			row = sheet.getRow(rowNumber);

			if (row == null) {
				// still need to complete this part
				// logger.info("row found row as null");
				continue;
			}

			for (int columnNumber = 0; columnNumber < 6; columnNumber++) {
				currentCell = row.getCell(columnNumber, Row.CREATE_NULL_AS_BLANK);
				if (currentCell.getColumnIndex() == 0 && currentCell.toString().equals(toCheck)) {
					logger.info("Writing for product " + currentCell.toString() + ", in lastVendor row  " + toWrite+" in excel");
					vendorCell = row.getCell(5);
					vendorCell.setCellValue(toWrite);
					break outer;
				}
			}
		}
	}

	public static void genrateExcel() {
		try {
			FileOutputStream outputStream = new FileOutputStream(new File("genratedExcel.xlsx"));
			workbook.write(outputStream);
			workbook.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	

}
