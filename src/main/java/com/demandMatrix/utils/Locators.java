package com.demandMatrix.utils;

public class Locators {

	public class BasePage {
		public static final String homeLoGoLocator = "home_img_holder";
	}

	public class LandingPage {
		public static final String googleLogo = "hplogo";
		public static final String searchBox = "q";
	}

	public class SearchResultPage {
		public static final String pageValidationLocator = "//div[contains(text(),'All')]";
	}

}
