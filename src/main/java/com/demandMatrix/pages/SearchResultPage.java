package com.demandMatrix.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.demandMatrix.utils.BaseWebdriver;
import com.demandMatrix.utils.ExcelTestData;
import com.demandMatrix.utils.FunctionLib;
import com.demandMatrix.utils.Locators;

public class SearchResultPage extends BasePage {

	@FindBy(xpath = Locators.SearchResultPage.pageValidationLocator)
	private WebElement pageValidationl;

	SearchResultPage() {
		PageFactory.initElements(BaseWebdriver.getDriver(), this);
		//This is just to avoid google automation validation 
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
					}
		FunctionLib.isTitlePresent("- Google Search");
		Assert.assertTrue(FunctionLib.isElemntVisble(pageValidationl),
			"Failed as could not find \"All\" link in search result page" + this);
	}

	public boolean validateResult(String toBeFind, String url) {
		boolean flag = false;
		String xpathURL = "//cite[contains(.,'" + url + "')]/ancestor::div[@class='rc']/h3/a";
		flag = verifyProductResult(By.xpath(xpathURL));
		if (!flag) {
			findVendorMatch(toBeFind);
		}
		return flag;
	}

	private void findVendorMatch(String toBeFind) {
		List<WebElement> allElements = BaseWebdriver.getDriver().findElements(By.xpath("//h3"));
		outer: for (WebElement element : allElements) {
			String urlFromTitle = element.findElement(By.xpath("//following-sibling::div//cite")).getText();
			inner: if (element.getText().contains(toBeFind) && (!element.getText().contains("Wikipedia"))) {
				if (urlFromTitle.contains("wikipedia.org"))
					break inner;
				ExcelTestData.writeInExcel(toBeFind, getLastVendor(urlFromTitle));
				break outer;
			}
			if (element.getText().equals(allElements.get(allElements.size() - 1).getText()))
				logger.info("Could not find last vendor name for " + toBeFind);
		}
	}

}
