package com.demandMatrix.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.demandMatrix.utils.BaseWebdriver;
import com.demandMatrix.utils.FunctionLib;
import com.demandMatrix.utils.Locators;

public class LandingPage extends BasePage {

	@FindBy(name = Locators.LandingPage.searchBox)
	WebElement searchBoxElement;
	
	@FindBy(id = Locators.LandingPage.googleLogo)
	WebElement googleLogo;

	

	public LandingPage() {
		PageFactory.initElements(BaseWebdriver.getDriver(), this);
		Assert.assertTrue(FunctionLib.isElemntVisble(googleLogo),
				"Failed as could not find logo of google in landing page" + this);
	}

	public SearchResultPage navigateToResultPage(String searchOption) {
		Assert.assertTrue(FunctionLib.isElemntVisble(searchBoxElement));
		searchBoxElement.clear();
		searchBoxElement.sendKeys(searchOption+Keys.RETURN);
		return new SearchResultPage();
	}
	
	
}
