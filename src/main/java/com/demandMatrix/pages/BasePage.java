package com.demandMatrix.pages;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.demandMatrix.utils.BaseWebdriver;
import com.demandMatrix.utils.Configuration;
import com.demandMatrix.utils.LoggerClass;

public class BasePage {

	protected static Properties prop;
	protected static final Logger logger = LoggerClass.createLogger();

	public BasePage() {
		PageFactory.initElements(BaseWebdriver.getDriver(), this);
	}

	public static boolean verifyProductResult(By alocator) {
		try {
			BaseWebdriver.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(BaseWebdriver.getDriver(), 3);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(alocator));
			BaseWebdriver.getDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			return true;
		} catch (Exception e) {
			BaseWebdriver.getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
			return false;
		}
	}

	public static String getLastVendor(String atext) {
		List<String> tempValue = new LinkedList<String>();
		Pattern myPattern = Pattern.compile("[https://[A-Za-z0-9]{0,}.]+[A-Za-z0-9]+\\.[A-Za-z]{2,3}");
		Matcher myMatch = myPattern.matcher(atext);
		while (myMatch.find()) {
			if (myMatch.group().length() != 0) {
				tempValue.add(myMatch.group().trim());
			}
		}

		return removeUrls(tempValue.get(0)).toString();
	}

	private static StringBuffer removeUrls(String tempValue) {
		StringBuffer text = new StringBuffer(tempValue);
		if (text.toString().contains("www.")) {
			String temp = "www.";
			text = text.delete(0, text.toString().indexOf(temp) + temp.length());
		} else if (text.toString().contains("https://")) {
			String temp = "https://";
			text = text.delete(0, text.toString().indexOf(temp) + temp.length());
		}
		return text;
	}

}
